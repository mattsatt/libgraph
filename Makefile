# Project Settings
NS_NAME := stx
LIB_NAME := graph

# Build Settings
CXX := g++
CXXFLAGS := -Wall -Wextra -std=c++20
LDFLAGS := -I include

BIN_DIR := bin
OBJ_DIR := obj
TEST_DIR := test

INSTALL_LIB_DIR := /usr/local/lib
INSTALL_INCLUDE_DIR := /usr/local/include

SOURCES := $(shell find $(TEST_DIR)/ -type f -name '*.cxx')
OBJECTS := $(patsubst $(TEST_DIR)/%.cxx, $(OBJ_DIR)/%.o, $(SOURCES))
TARGETS := $(patsubst $(OBJ_DIR)/%.o, $(BIN_DIR)/%, $(OBJECTS))

.PHONY: all clean install uninstall

all: $(TARGETS)

clean:
	@rm -rf $(BIN_DIR)
	@rm -rf $(OBJ_DIR)

install:
	@mkdir -p $(INSTALL_LIB_DIR)/$(NS_NAME)/$(LIB_NAME)
	@cp -R include/* $(INSTALL_LIB_DIR)/$(NS_NAME)/$(LIB_NAME)
	@mkdir -p $(INSTALL_INCLUDE_DIR)/$(NS_NAME)
	@ln -fs $(INSTALL_LIB_DIR)/$(NS_NAME)/$(LIB_NAME)/$(LIB_NAME).hxx $(INSTALL_INCLUDE_DIR)/$(NS_NAME)/$(LIB_NAME)

uninstall:
	@rm $(INSTALL_INCLUDE_DIR)/$(NS_NAME)/$(LIB_NAME)
	@rm -rf $(INSTALL_LIB_DIR)/$(NS_NAME)/$(LIB_NAME)

$(BIN_DIR)/%: $(OBJ_DIR)/%.o
	@mkdir -p $(BIN_DIR)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^

$(OBJ_DIR)/%.o: $(TEST_DIR)/%.cxx
	@mkdir -p $(OBJ_DIR)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -c -o $@ $<
