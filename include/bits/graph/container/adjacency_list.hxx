#ifndef LIBGRAPH_ADJACENCY_LIST
#define LIBGRAPH_ADJACENCY_LIST

#include <algorithm>
#include <map>
#include <memory>
#include <ranges>
#include <set>

namespace stdx::graph {
    template <typename Node, typename Edge, template <typename> typename Container>
    class basic_adjacency_list {
    public:
        /// @brief The node type.
        using node_type = Node;

        /// @brief The edge type.
        using edge_type = Edge;

        /// @brief The size type.
        using size_type = std::size_t;

        /// @brief Determines if the graph contains the node.
        /// @param node The node.
        bool contains_node(const node_type& node) const {
            auto it = std::ranges::find(data_, node, [](auto&& x) { return *x.first; });
            return it != std::ranges::end(data_);
        }

        /// @brief Determines if the graph contains the edge.
        /// @param from The from node.
        /// @param to The to node.
        bool contains_edge(const node_type& from, const node_type& to) const {
            if (contains_node(from) && contains_node(to)) {
                auto from_it = std::ranges::find(data_, from, [](auto&& x) { return *x.first; });
                if (from_it != std::ranges::end(data_)) {
                    auto to_it = std::ranges::find(from_it->second, to, [](auto&& x) { return *x.first; });
                    return to_it != std::ranges::end(from_it->second);
                }
            }
            return false;
        }

        /// @brief Determines if the graph is empty.
        bool empty() const noexcept {
            return data_.empty();
        }

        /// @brief Returns the number of nodes.
        size_type nodes() const noexcept {
            return data_.size();
        }

        /// @brief Returns the number of edges.
        size_type edges() const {
            size_type count = 0;
            std::ranges::for_each(data_, [&](auto&& x) { count += x.second.size(); });
            return count;
        }

        /// @brief Clears the contents.
        void clear() {
            data_.clear();
        }

        /// @brief Insert a node into the graph.
        /// @tparam T The node type.
        /// @param node The node.
        template <typename T>
        std::shared_ptr<node_type> insert_node(T&& node) {
            if (!contains_node(node)) {
                auto pair = data_.emplace(std::make_shared<node_type>(std::forward<T>(node)), Container<
                    std::pair<
                        std::shared_ptr<node_type>,
                        std::shared_ptr<edge_type>
                    >
                >());
                return pair.first->first;
            } else {
                auto it = std::ranges::find(data_, node, [](auto&& x) { return *x.first; });
                return it->first;
            }
        }

        /// @brief Inserts an edge into the graph.
        /// @tparam T The from node type.
        /// @tparam U The to node type.
        /// @param from The from node.
        /// @param to The to node.
        template <typename T, typename U, typename V>
        void insert_edge(T&& from, U&& to, V&& edge) {
            // auto from_ptr = insert_node(std::forward<T>(from));
            // auto to_ptr = insert_node(std::forward<U>(to));
            // auto edge_ptr = std::make_shared(std::forward<V>(edge));
            // auto link = std::make_pair(to_ptr, edge_ptr);
            // data_.at(from_ptr).insert(std::move(link));
        }

        template <typename T, typename U>
        void copy_edge(const node_type& a_from, const node_type& a_to, T&& b_from, U&& b_to) {

        }

        /// @brief Removes a node from the graph.
        /// @param node The node to remove.
        void remove_node(const node_type& node) {
            // for (auto& link : data_) {
            //     auto it = std::ranges::find(link.second, node, [](auto&& x) { return x.first; });
            //     if (it != std::ranges::end(link.second)) {
            //         link.second.erase(it);
            //     }
            // }
            // data_.erase(node);
        }

        /// @brief Remove an edge from the graph.
        /// @param from The from node.
        /// @param to The to node.
        void remove_edge(const node_type& from, const node_type& to) {
            // if (contains_edge(from, to)) {
            //     auto it = std::ranges::find(data_.at(from), to, [](auto&& x) { return x.first; })
            //     data_.at(from).erase(it);
            // }
        }

    private:
        std::map<std::shared_ptr<node_type>, Container<
            std::pair<
                std::shared_ptr<node_type>,
                std::shared_ptr<edge_type>
            >
        >> data_;
    };

    template <typename Node, typename Edge>
    using adjacency_list = basic_adjacency_list<Node, Edge, std::set>;

    template <typename Node, typename Edge>
    using multi_adjacency_list = basic_adjacency_list<Node, Edge, std::multiset>;
}

#endif
