#ifndef LIBGRAPH_DIGRAPH
#define LIBGRAPH_DIGRAPH

#include "container/adjacency_list.hxx"
#include "iterator/edge_traits.hxx"
#include "iterator/node_traits.hxx"

namespace stdx::graph {
    /// @brief A directed graph.
    /// @tparam Node The node type.
    /// @tparam Edge The edge type.
    template <typename Node, typename Edge, typename AdjacencyContainer = adjacency_list<Node, Edge>>
    class digraph {
    public:
        using node_type = typename AdjacencyContainer::node_type;

        using edge_type = typename AdjacencyContainer::edge_type;

        using size_type = typename AdjacencyContainer::size_type;

        bool contains_node(const node_type& node) const {
            return data_.contains_node(node);
        }

        bool contains_edge(const node_type& from, const node_type& to) const {
            return data_.contains_edge(from, to);
        }

        bool empty() const noexcept {
            return data_.empty();
        }

        size_type nodes() const noexcept {
            return data_.nodes();
        }

        size_type edges() const {
            return data_.edges();
        }

        void clear() {
            data_.clear();
        }

        template <typename T>
        void insert_node(T&& node) {
            data_.insert_node(std::forward<T>(node));
        }

        template <typename T, typename U, typename V>
        void insert_edge(T&& from, U&& to, V&& edge) {
            data_.insert_edge(std::forward<T>(from), std::forward<U>(to), std::forward<V>(edge));
        }

        void remove_node(const node_type& node) {
            data_.remove_node(node);
        }

        void remove_edge(const node_type& from, const node_type& to) {
            data_.remove_edge(from, to);
        }

    private:
        AdjacencyContainer data_;
    };
}

#endif
