#ifndef LIBGRAPH_EDGE_ITERATOR
#define LIBGRAPH_EDGE_ITERATOR

namespace stdx::graph {
    template <typename Graph>
    class edge_iterator {
    public:
        using value_type = std::pair<
            typename Graph::node_type,
            typename Graph::node_type
        >;

        using pointer = const value_type*;
            
        using const_pointer = const pointer;
            
        using reference = const value_type&;
            
        using const_reference = const reference;

        edge_iterator() = default;

        edge_iterator(const Graph& graph)
            : graph_(&graph)
            , n_it_(node_traits<Graph>::at(*graph_, 0))
            , e_it_(edge_traits<Graph>::at(*graph_, 0, 0))
            , value_(std::make_pair(node_traits<Graph>::get(n_it_), edge_traits<Graph>::get(e_it_))) {
        }

        reference operator*() {
            return value_;
        }

        const_reference operator*() const {
            return value_;
        }

        pointer operator->() {
            return &value_;
        }

        const_pointer operator->() const {
            return &value_;
        }
    
        edge_iterator& operator++() {
            if (edge_traits<Graph>::next(*graph_, n_it_, e_it_)) {
                auto from = node_traits<Graph>::get(n_it_);
                auto to = edge_traits<Graph>::get(e_it_);
                value_ = std::make_pair(from, to);
            } else {
                *this = edge_iterator();
            }
            return *this;
        }

        edge_iterator operator++(int) {
            auto tmp = *this;
            ++*this;
            return tmp;
        }

        edge_iterator& operator--() {
            if (edge_traits<Graph>::prev(*graph_, n_it_, e_it_)) {
                auto from = node_traits<Graph>::get(n_it_);
                auto to = edge_traits<Graph>::get(e_it_);
                value_ = std::make_pair(from, to);
            } else {
                *this = edge_iterator();
            }
            return *this;
        }

        edge_iterator operator--(int) {
            auto tmp = *this;
            --*this;
            return tmp;
        }

        friend bool operator==(const edge_iterator&, const edge_iterator&) = default;

    private:
        const Graph* graph_;
        
        typename node_traits<Graph>::const_iterator n_it_;

        typename edge_traits<Graph>::const_iterator e_it_;

        value_type value_;
    };

    template <typename Graph>
    auto begin(edge_iterator<Graph>& it) noexcept {
        return it;
    }

    template <typename Graph>
    auto end(edge_iterator<Graph>&) noexcept {
        return edge_iterator<Graph>();
    }
}

#endif
