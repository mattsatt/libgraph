#ifndef LIBGRAPH_EDGE_TRAITS
#define LIBGRAPH_EDGE_TRAITS

#include <iterator>
#include "node_traits.hxx"

namespace stdx::graph {
    template <typename Graph>
    class edge_traits {
    public:
        using iterator = typename decltype(std::declval<Graph>().data_)::iterator::value_type::second_type::iterator;

        using const_iterator = typename decltype(std::declval<Graph>().data_)::const_iterator::value_type::second_type::const_iterator;

        static auto at(const Graph& graph, std::size_t node_idx, std::size_t edge_idx) {
            auto node_it = node_traits<Graph>::at(graph, node_idx);
            return std::ranges::next(node_it->second.cbegin(), edge_idx);
        }

        template <typename EdgeIt>
        static auto& get(EdgeIt edge_it) {
            return *edge_it;
        }

        template <typename NodeIt, typename EdgeIt>
        static bool next(const Graph& graph, NodeIt& n_it, EdgeIt& e_it) {
            auto& nodes = graph.data_;
            if (n_it != nodes.cend()) {
                auto& edges = n_it->second;
                if (e_it != edges.cend() && ++e_it != edges.cend()) {
                    return true;
                }

                while (n_it != nodes.cend() && ++n_it != nodes.cend()) {
                    if (!n_it->second.empty()) {
                        e_it = n_it->second.cbegin();
                        return true;
                    }
                }
            }

            n_it = NodeIt();
            e_it = EdgeIt();
            return false;
        }

        template <typename NodeIt, typename EdgeIt>
        static bool prev(const Graph& graph, NodeIt& n_it, EdgeIt& e_it) {
            auto& nodes = graph.data_;
            if (n_it != nodes.crend()) {
                auto& edges = n_it->second;
                if (e_it != edges.crend() && ++e_it != edges.crend()) {
                    return true;
                }

                while (n_it != nodes.crend() && ++n_it != nodes.crend()) {
                    if (!n_it->second.empty()) {
                        e_it = n_it->second.crbegin();
                        return true;
                    }
                }
            }

            n_it = NodeIt();
            e_it = EdgeIt();
            return false;
        }
    };
}

#endif
