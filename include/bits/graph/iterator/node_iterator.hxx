#ifndef LIBGRAPH_NODE_ITERATOR
#define LIBGRAPH_NODE_ITERATOR

#include <compare>

namespace stdx::graph {
    template <typename Graph>
    class node_iterator {
    public:
        using value_type = typename Graph::node_type;

        using pointer = const value_type*;
            
        using const_pointer = const pointer;
            
        using reference = const value_type&;
            
        using const_reference = const reference;

        node_iterator() = default;

        node_iterator(const Graph& graph)
            : graph_(&graph), it_(node_traits<Graph>::at(*graph_, 0)) {
        }

        reference operator*() {
            return node_traits<Graph>::get(it_);
        }

        const_reference operator*() const {
            return node_traits<Graph>::get(it_);
        }

        pointer operator->() {
            return &node_traits<Graph>::get(it_);
        }

        const_pointer operator->() const {
            return &node_traits<Graph>::get(it_);
        }
    
        node_iterator& operator++() {
            if (!node_traits<Graph>::next(*graph_, it_)) {
                *this = node_iterator();
            }
            return *this;
        }

        node_iterator operator++(int) {
            auto tmp = *this;
            ++*this;
            return tmp;
        }

        node_iterator& operator--() {
            if (!node_traits<Graph>::prev(*graph_, it_)) {
                *this = node_iterator();
            }
            return *this;
        }

        node_iterator operator--(int) {
            auto tmp = *this;
            --*this;
            return tmp;
        }

        friend bool operator==(const node_iterator&, const node_iterator&) = default;

    private:
        const Graph* graph_;
        
        typename node_traits<Graph>::const_iterator it_;
    };

    template <typename Graph>
    auto begin(node_iterator<Graph>& it) noexcept {
        return it;
    }

    template <typename Graph>
    auto end(node_iterator<Graph>&) noexcept {
        return node_iterator<Graph>();
    }
}

#endif
