#ifndef LIBGRAPH_NODE_ITERATOR_TRAITS
#define LIBGRAPH_NODE_ITERATOR_TRAITS

#include <iterator>

namespace stdx::graph {
    template <typename Graph>
    class node_traits {
    public:
        using iterator = typename decltype(std::declval<Graph>().data_)::iterator;

        using const_iterator = typename decltype(std::declval<Graph>().data_)::const_iterator;

        static auto at(const Graph& graph, std::size_t node_idx) {
            return std::ranges::next(graph.data_.cbegin(), node_idx);
        }

        template <typename NodeIt>
        static auto& get(NodeIt node_it) {
            return node_it->first;
        }

        template <typename NodeIt>
        static bool next(const Graph& graph, NodeIt& node_it) {
            return node_it != graph.data_.cend() && ++node_it != graph.data_.cend();
        }

        template <typename NodeIt>
        static bool prev(const Graph& graph, NodeIt& node_it) {
            return node_it != graph.data_.crend() && ++node_it != graph.data_.crend();
        }
    };
}

#endif
