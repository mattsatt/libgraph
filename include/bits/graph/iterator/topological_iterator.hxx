#ifndef LIBGRAPH_TOPOLOGICAL_ITERATOR
#define LIBGRAPH_TOPOLOGICAL_ITERATOR

namespace stdx::graph {
    /// @brief Provide a list of nodes topologically sorted.
    /// @tparam T The node type.
    /// @tparam OutputIt The output iterator type.
    /// @param graph The digraph.
    /// @param it The output iterator type.
    // template <typename T, typename OutputIt>
    // void topological_sort(const digraph<T>& graph, OutputIt it) {
    //     std::map<T, std::size_t> in_degrees;
    //     for (const auto& node : graph.nodes()) {
    //         in_degrees.insert({node, 0});
    //     }
    //     for (const auto& edge : graph.edges()) {
    //         ++in_degrees[edge.second];
    //     }

    //     // Track nodes with no incoming edges
    //     std::deque<T> no_incoming;
    //     for (const auto& entry : in_degrees) {
    //         if (entry.second == 0) {
    //             no_incoming.push_back(entry.first);
    //         }
    //     }
        
    //     // As long as there are nodes with no incoming edges that can be added to the ordering
    //     while (!no_incoming.empty()) {
    //         auto next = no_incoming.front();
    //         no_incoming.pop_front();
    //         it++ = next;
    //         for (const auto& edge : graph.outgoing(next)) {
    //             --in_degrees[edge.second];
    //             if (in_degrees[edge.second] == 0) {
    //                 no_incoming.push_back(edge.second);
    //             }
    //         }
    //     }
    // }
}

#endif
