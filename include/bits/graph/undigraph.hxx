#ifndef LIBGRAPH_UNDIGRAPH
#define LIBGRAPH_UNDIGRAPH

#include "container/adjacency_list.hxx"
#include "iterator/edge_traits.hxx"
#include "iterator/node_traits.hxx"

namespace stdx::graph {
    /// @brief An undirected graph.
    /// @tparam T The value type.
    template <typename Node, typename Edge, typename AdjacencyContainer = adjacency_list<Node, Edge>>
    class undigraph {
    public:
        using node_type = typename AdjacencyContainer::node_type;

        using edge_type = typename AdjacencyContainer::edge_type;

        using size_type = typename AdjacencyContainer::size_type;

        bool contains_node(const node_type& node) const {
            return data_.contains_node(node);
        }

        bool contains_edge(const node_type& from, const node_type& to) const {
            return data_.contains_edge(from, to) && data_.contains_edge(to, from);
        }

        bool empty() const noexcept {
            return data_.empty();
        }

        size_type nodes() const noexcept {
            return data_.nodes();
        }

        size_type edges() const {
            return data_.edges();
        }

        void clear() {
            data_.clear();
        }

        template <typename T>
        void insert_node(T&& node) {
            data_.insert_node(std::forward<T>(node));
        }

        template <typename T, typename U, typename V>
        void insert_edge(T&& from, U&& to, V&& edge) {
            data_.insert_edge(from, to, std::forward<V>(edge));
            data_.copy_edge(from, to, to, from);
        }

        void remove_node(const node_type& node) {
            data_.remove_node(node);
        }

        void remove_edge(const node_type& from, const node_type& to) {
            data_.remove_edge(from, to);
            data_.remove_edge(to, from);
        }

    private:
        AdjacencyContainer data_;
    };
}

#endif
