#include <cassert>
#include <graph.hxx>

using namespace stdx::graph;

void test_copy_constructor() {
    digraph<int, int> graph;
    graph.insert_edge(1, 2, 0);

    digraph copy(graph);
    copy.remove_node(2);
    assert(graph.contains_node(1));
    assert(graph.contains_node(2));
    assert(graph.contains_edge(1, 2));
    assert(copy.contains_node(1));
    assert(!copy.contains_node(2));
    assert(!copy.contains_edge(1, 2));
}

void test_contains_node() {
    digraph<int, int> graph;
    graph.insert_node(1);
    assert(graph.contains_node(1));
}

void test_empty() {
    digraph<int, int> graph;
    assert(graph.empty());
    graph.insert_node(1);
    graph.remove_node(1);
    assert(graph.empty());
}

void test_node_size() {
    digraph<int, int> graph;
    graph.insert_node(1);
    graph.insert_node(1);
    assert(graph.nodes() == 1);
    graph.insert_node(2);
    assert(graph.nodes() == 2);
}

void test_insert_node() {
    digraph<int, int> graph;
    int value = 1;
    graph.insert_node(value);
    graph.insert_node(2);
    assert(graph.contains_node(1));
    assert(graph.contains_node(2));
}

void test_remove_node() {
    digraph<int, int> graph;
    graph.insert_node(1);
    assert(graph.contains_node(1));
    graph.remove_node(1);
    assert(!graph.contains_node(1));
    assert(graph.empty());
}

void test_contains_edge() {
    digraph<int, int> graph;
    graph.insert_edge(1, 2, 0);
    assert(graph.contains_edge(1, 2));
    assert(!graph.contains_edge(2, 1));
}

void test_edge_size() {
    digraph<int, int> graph;
    graph.insert_edge(1, 2, 0);
    graph.insert_edge(2, 1, 0);
    assert(graph.edges() == 2);
    graph.remove_node(2);
    assert(graph.edges() == 0);
}

void test_insert_edge() {
    digraph<int, int> graph;
    int value1 = 1;
    int value2 = 2;
    graph.insert_edge(value1, value2, 0);
    graph.insert_edge(value1, 2, 0);
    graph.insert_edge(1, value2, 0);
    graph.insert_edge(1, 2, 0);
    assert(graph.contains_edge(1, 2));
}

void test_remove_edge() {
    digraph<int, int> graph;
    graph.insert_edge(1, 2, 0);
    assert(graph.contains_edge(1, 2));
    assert(!graph.contains_edge(2, 1));
    graph.remove_edge(2, 1);
    assert(graph.contains_edge(1, 2));
    assert(!graph.contains_edge(2, 1));
    assert(graph.nodes() == 2);
}

int main() {
    test_copy_constructor();
    test_contains_node();
    test_empty();
    test_node_size();
    test_insert_node();
    test_remove_node();
    test_contains_edge();
    test_edge_size();
    test_insert_edge();
    test_remove_edge();
}
