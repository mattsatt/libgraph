#include <cassert>
#include <iostream>
#include <graph.hxx>

using namespace stdx::graph;

template <typename T>
void test_graph_forward() {
    T graph;
    graph.insert_edge(1, 2, 'a');
    graph.insert_edge(2, 3, 'b');
    graph.insert_edge(3, 1, 'b');

    for (const auto& node : edge_iterator(graph)) {
        std::cout << node.first << ',' << node.second << '\n';
    }
}

int main(int argc, char** argv) {
    test_graph_forward<digraph<int, char>>();
    test_graph_forward<undigraph<int, char>>();
}
