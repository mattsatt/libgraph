#include <cassert>
#include <iostream>
#include <graph.hxx>

using namespace stdx::graph;

template <typename T>
void test_graph_forward() {
    T graph;
    graph.insert_node(1);
    graph.insert_node(2);
    graph.insert_node(3);
    graph.insert_edge(1, 2, 'a');
    graph.insert_edge(2, 3, 'b');
    graph.insert_edge(3, 1, 'c');

    int x = 1;
    for (const auto& node : node_iterator(graph)) {
        assert(node == x++);
    }
}

int main(int argc, char** argv) {
    test_graph_forward<digraph<int, char>>();
    test_graph_forward<undigraph<int, char>>();
}
