#include <cassert>
#include <graph.hxx>

using namespace stdx::graph;

void test_copy_constructor() {
    undigraph<int, char> graph;
    graph.insert_edge(1, 2, 'a');

    undigraph copy(graph);
    copy.remove_node(2);
    assert(graph.contains_node(1));
    assert(graph.contains_node(2));
    assert(graph.contains_edge(1, 2));
    assert(copy.contains_node(1));
    assert(!copy.contains_node(2));
    assert(!copy.contains_edge(1, 2));
}

void test_contains_node() {
    undigraph<int, char> graph;
    graph.insert_node(1);
    assert(graph.contains_node(1));
}

void test_empty() {
    undigraph<int, char> graph;
    assert(graph.empty());
    graph.insert_node(1);
    graph.remove_node(1);
    assert(graph.empty());
}

void test_node_size() {
    undigraph<int, char> graph;
    graph.insert_node(1);
    graph.insert_node(1);
    assert(graph.nodes() == 1);
    graph.insert_node(2);
    assert(graph.nodes() == 2);
}

void test_insert_node() {
    undigraph<int, char> graph;
    int value = 1;
    graph.insert_node(value);
    graph.insert_node(2);
    assert(graph.contains_node(1));
    assert(graph.contains_node(2));
}

void test_remove_node() {
    undigraph<int, char> graph;
    graph.insert_node(1);
    assert(graph.contains_node(1));
    graph.remove_node(1);
    assert(!graph.contains_node(1));
    assert(graph.empty());
}

void test_contains_edge() {
    undigraph<int, char> graph;
    graph.insert_edge(1, 2, 'a');
    assert(graph.contains_edge(1, 2));
    assert(graph.contains_edge(2, 1));
}

void test_edge_size() {
    undigraph<int, char> graph;
    graph.insert_edge(1, 2, 'a');
    graph.insert_edge(2, 1, 'b');
    assert(graph.edges() == 1);
    graph.remove_node(2);
    assert(graph.edges() == 0);
}

void test_insert_edge() {
    undigraph<int, char> graph;
    int value1 = 1;
    int value2 = 2;
    graph.insert_edge(value1, value2, 'a');
    graph.insert_edge(value1, 2, 'b');
    graph.insert_edge(1, value2, 'c');
    graph.insert_edge(1, 2, 'd');
    assert(graph.contains_edge(1, 2));
}

void test_remove_edge() {
    undigraph<int, char> graph;
    graph.insert_edge(1, 2, 'a');
    assert(graph.contains_edge(1, 2));
    assert(graph.contains_edge(2, 1));
    graph.remove_edge(2, 1);
    assert(!graph.contains_edge(1, 2));
    assert(!graph.contains_edge(2, 1));
    assert(graph.nodes() == 2);
}

int main() {
    test_copy_constructor();
    test_contains_node();
    test_empty();
    test_node_size();
    test_insert_node();
    test_remove_node();
    test_contains_edge();
    test_edge_size();
    test_insert_edge();
    test_remove_edge();
}
